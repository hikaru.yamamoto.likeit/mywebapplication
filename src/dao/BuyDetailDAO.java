package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;

public class BuyDetailDAO {

	//購入詳細登録
	public static void insertBuyDetail(BuyDetailDataBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO buy_detail(buy_id,item_id) VALUES(?,?)");
			st.setInt(1, bddb.getBuyId());
			st.setInt(2, bddb.getItemId());
			st.executeUpdate();
			System.out.println("購入詳細情報登録");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//購入IDによる購入情報検索
	public ArrayList<BuyDetailDataBeans> getBuyDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM buy_detail WHERE buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<BuyDetailDataBeans> buyDetailList = new ArrayList<BuyDetailDataBeans>();

			while (rs.next()) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setId(rs.getInt("id"));
				bddb.setBuyId(rs.getInt("buy_id"));
				bddb.setItemId(rs.getInt("item_id"));
				buyDetailList.add(bddb);
			}

			System.out.println("購入IDによる購入情報検索");
			return buyDetailList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	 //購入IDによる商品情報詳細検索
	public static ArrayList<ItemDataBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM buy_detail JOIN item ON buy_detail.item_id = item.id WHERE buy_detail.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> buyDetailItemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();

				idb.setId(rs.getInt("item.id"));
				idb.setName(rs.getString("item.name"));
				idb.setPrice(rs.getInt("item.price"));
				idb.setAddDate(rs.getDate("create_date"));
				idb.setFileName(rs.getString("file_name"));
				idb.setCategory(rs.getString("category"));
				idb.setDetail(rs.getString("detail"));


				buyDetailItemList.add(idb);
			}

			System.out.println("購入IDによる購入情報詳細検索");
			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//商品IDで購入情報を取得
		public static BuyDetailDataBeans getBuydetailByItemID(int itemId) {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				st = con.prepareStatement("SELECT * FROM buy_detail WHERE item_id = ?");
				st.setInt(1, itemId);

				ResultSet rs = st.executeQuery();

				BuyDetailDataBeans bdbd = new BuyDetailDataBeans();
				if (rs.next()) {
					bdbd.setId(rs.getInt("id"));

					}

				System.out.println("商品IDによる購入情報の取得");

				return bdbd;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {

				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
}
