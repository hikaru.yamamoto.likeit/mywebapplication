package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;

public class UserDAO {

	//ログイン
	public UserDataBeans LoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, userenc(password));
			ResultSet rs = pStmt.executeQuery();


			UserDataBeans udb = new UserDataBeans();
			if (!rs.next()) {
				return null;
			}

				udb.setId(rs.getInt("id"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setName(rs.getString("name"));
				udb.setAddress(rs.getString("address"));
				udb.setCreateDate(rs.getString("create_date"));


			System.out.println("ログインIDとパスワードによるユーザー検索");

			return udb;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ログインID検索
	public UserDataBeans findByLoginId(String loginId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			UserDataBeans udb = new UserDataBeans();


			if (!rs.next()) {
				return null;
			}

				udb.setLoginId(rs.getString("login_id"));
				udb.setId(rs.getInt("id"));

				System.out.println(rs.getString("login_id"));

			System.out.println("ログインIDによるユーザー検索");

			return udb;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//新規ユーザー登録
	public static void createUser(UserDataBeans udb) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String insql = "INSERT INTO user(login_id,name,address,password,create_date)VALUES(?,?,?,?,now())";

				PreparedStatement pStmt = conn.prepareStatement(insql);

				pStmt.setString(1, udb.getLoginId());
				pStmt.setString(2, udb.getName());
				pStmt.setString(3, udb.getAddress());
				pStmt.setString(4, userenc(udb.getPassword()));

				pStmt.executeUpdate();

				System.out.println("新規ユーザー登録");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//全ユーザー情報取得
	public static List<UserDataBeans> findAll() {
		Connection conn = null;
		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id !='admin'";

			//WHERE login_id!='admin

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {

				UserDataBeans user = new UserDataBeans();

				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setName(rs.getString("name"));
				user.setAddress(rs.getString("address"));
				user.setPassword(rs.getString("password"));
				user.setCreateDate(rs.getString("create_date"));

				userList.add(user);
			}

			System.out.println("全ユーザー情報取得");
			return userList;



		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	//名前か住所によるユーザー検索
	public List<UserDataBeans> userfind(String name,String address) {
		Connection conn = null;
		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id !='admin'";

			if(!name.equals("")) {
				sql+=" AND name ='"+name+"'";
			}

			if(!address.equals("")) {
				sql+=" AND address like '%"+address+"%'";
			}


			Statement stmt = conn.createStatement();
	        ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				UserDataBeans user = new UserDataBeans();

				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setName(rs.getString("name"));
				user.setAddress(rs.getString("address"));
				user.setPassword(rs.getString("password"));
				user.setCreateDate(rs.getString("create_date"));

				userList.add(user);

			}

			System.out.println("名前か住所によるユーザー検索");
			return userList;



		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

					}
				}
			}
		}

	//ユーザ情報削除
	public void userDelete(int id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String delsql = "DELETE FROM user WHERE id =?";

				PreparedStatement pStmt = conn.prepareStatement(delsql);

				pStmt.setInt(1, id);

				pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

					}
				}
			}
		}

	//ユーザIDによるユーザ情報取得
		public UserDataBeans userRefe(int id) {
			Connection conn = null;
			try {
				conn = DBManager.getConnection();

				String sql = "SELECT * FROM user WHERE id = ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1, id);

				ResultSet rs = pStmt.executeQuery();

				UserDataBeans udb = new UserDataBeans();

				if (rs.next()) {

					udb.setId(rs.getInt("id"));
					udb.setLoginId(rs.getString("login_id"));
					udb.setName(rs.getString("name"));
					udb.setAddress(rs.getString("address"));
					udb.setCreateDate(rs.getString("create_date"));
					udb.setPassword(rs.getString("password"));

				}
				System.out.println("ユーザIDによるユーザ情報取得");

				return udb;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}


		//ユーザ情報更新
				public static void userUpdate(UserDataBeans udb) {
					Connection conn = null;

					try {
						conn = DBManager.getConnection();

						String upsql = "UPDATE user SET name=?,address=?,login_id=?,password=? WHERE id=?";

							PreparedStatement pStmt = conn.prepareStatement(upsql);

							pStmt.setString(1, udb.getName());
							pStmt.setString(2, udb.getAddress());
							pStmt.setString(3, udb.getLoginId());
							pStmt.setString(4, userenc(udb.getPassword()));
							pStmt.setInt(5, udb.getId());

							pStmt.executeUpdate();
							System.out.println("ユーザ情報更新完了");

					} catch (SQLException e) {
						e.printStackTrace();
					} finally {

						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
					}
				}

				//商品IDによるユーザ名取得
				public static UserDataBeans getItemIdByUsername(int itemId) {
					Connection conn = null;
					try {
						conn = DBManager.getConnection();

						String sql = "SELECT * FROM user JOIN item ON user.id = item.user_id WHERE item.id = ?";

						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setInt(1, itemId);

						ResultSet rs = pStmt.executeQuery();

						UserDataBeans udb = new UserDataBeans();

						if (rs.next()) {

							udb.setName(rs.getString("name"));

						}
						System.out.println("商品IDによるユーザ名取得");

						return udb;

					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
				}

				//ログインIDとパスワードによるユーザIDの取得
				public static int getUserId(String loginId, String password) throws SQLException {
					Connection con = null;
					PreparedStatement st = null;
					try {
						con = DBManager.getConnection();

						st = con.prepareStatement("SELECT * FROM user WHERE login_id = ?");
						st.setString(1, loginId);

						ResultSet rs = st.executeQuery();

						int userId = 0;
						while (rs.next()) {

								userId = rs.getInt("id");
								break;

						}

						System.out.println("ログインIDとパスワードによるユーザIDの取得");
						return userId;
					} catch (SQLException e) {
						System.out.println(e.getMessage());
						throw new SQLException(e);
					} finally {
						if (con != null) {
							con.close();
						}
					}
				}

				public static String userenc(String password) {

					String source = password;
					Charset charset = StandardCharsets.UTF_8;
					String algorithm = "MD5";

					byte[] bytes = null;
					try {
						bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
					} catch (NoSuchAlgorithmException e) {

						e.printStackTrace();
					}
					String result = DatatypeConverter.printHexBinary(bytes);
					System.out.println("暗号化"+result);

					return result;

					}



}