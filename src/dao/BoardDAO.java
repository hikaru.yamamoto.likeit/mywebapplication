package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.BoardDataBeans;

public class BoardDAO {

		//商品IDでコメント情報を取得
		public static ArrayList<BoardDataBeans> getBoard(int itemId) {

			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				st = con.prepareStatement("SELECT b.id,b.user_id comment_user,b.board,i.user_id host_id,u.name,b.create_date FROM board b INNER JOIN item i ON b.item_id = i.id INNER JOIN user u ON b.user_id = u.id WHERE i.id=?");

				st.setInt(1, itemId);

				ResultSet rs = st.executeQuery();

				ArrayList<BoardDataBeans> commentList = new ArrayList<BoardDataBeans>();

				while (rs.next()) {
					BoardDataBeans bdb = new BoardDataBeans();

					bdb.setUserName(rs.getString("u.name"));
					bdb.setCommentUserId(rs.getInt("comment_user"));
					bdb.setCommentDetail(rs.getString("b.board"));
					bdb.setHostUserId(rs.getInt("host_id"));
					bdb.setId(rs.getInt("b.id"));
					bdb.setCommentDate(rs.getDate("create_date"));

					commentList.add(bdb);
				}

				System.out.println("商品IDでコメント情報取得");

				return commentList;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {

				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

		//コメント投稿
		public static BoardDataBeans comment(int itemId,int userId,String comment) {
			Connection conn = null;

			try {
				conn = DBManager.getConnection();

				String insql = "INSERT INTO board(user_id,board,item_id,create_date)VALUES(?,?,?,now())";

					PreparedStatement pStmt = conn.prepareStatement(insql);

					pStmt.setInt(1, userId);
					pStmt.setString(2, comment);
					pStmt.setInt(3, itemId);


					pStmt.executeUpdate();

					System.out.println("コメント登録");

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return null;
		}

		//コメント情報削除
		public static void commentDelete(int id) {
			Connection conn = null;

			try {
				conn = DBManager.getConnection();

				String delsql = "DELETE FROM board WHERE id =?";

					PreparedStatement pStmt = conn.prepareStatement(delsql);

					pStmt.setInt(1, id);

					pStmt.executeUpdate();
					System.out.println("コメント削除");

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

						}
					}
				}
			}
}
