package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemDAO {

	//商品全件取得
	public static ArrayList<ItemDataBeans> getItem() {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM item");


			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setAddDate(rs.getDate("create_date"));
				item.setCategory(rs.getString("category"));
				itemList.add(item);

			}
			System.out.println("商品全件取得");

			return itemList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//商品カテゴリーを取得
	public static ArrayList<ItemDataBeans> getItemCategory() {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT category FROM item GROUP BY category");


			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> categoryList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setCategory(rs.getString("category"));
				categoryList.add(item);
			}
			System.out.println("商品カテゴリーを取得");

			return categoryList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//商品IDで商品情報を取得
	public static ItemDataBeans getItemByItemID(int itemId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM item WHERE id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setCategory(rs.getString("category"));
				item.setUserId(rs.getInt("user_id"));
				item.setAddDate(rs.getDate("create_date"));
				}

			System.out.println("商品IDによる商品情報の取得");

			return item;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//検索ワードによる商品総数の取得
	public static double getItemCount(String searchWord) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from item where name like ?");
			st.setString(1, "%" + searchWord + "%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			System.out.println("検索ワードによる商品総数の取得");

			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

		//検索ワードによる商品名検索
		public List<ItemDataBeans> Itemfind(String searchWord) {
			Connection conn = null;
			List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			try {

				conn = DBManager.getConnection();

				String sql = "SELECT * FROM item WHERE name like '%"+searchWord+"%'";

				Statement stmt = conn.createStatement();
		        ResultSet rs = stmt.executeQuery(sql);

				while (rs.next()) {
					ItemDataBeans item = new ItemDataBeans();

					item.setId(rs.getInt("id"));
					item.setName(rs.getString("name"));
					item.setDetail(rs.getString("detail"));
					item.setPrice(rs.getInt("price"));
					item.setFileName(rs.getString("file_name"));
					item.setCategory(rs.getString("category"));
					item.setUserId(rs.getInt("user_id"));
					item.setAddDate(rs.getDate("create_date"));

					itemList.add(item);

				}

				System.out.println("検索ワードによる商品名検索");
				return itemList;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;

						}
					}
				}
			}

		//カテゴリー検索による商品名検索
				public List<ItemDataBeans> 	categoryfind(String searchWord) {
					Connection conn = null;
					List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

					try {

						conn = DBManager.getConnection();

						String sql = "SELECT * FROM item WHERE category like '%"+searchWord+"%'";

						Statement stmt = conn.createStatement();
				        ResultSet rs = stmt.executeQuery(sql);

						while (rs.next()) {
							ItemDataBeans item = new ItemDataBeans();

							item.setId(rs.getInt("id"));
							item.setName(rs.getString("name"));
							item.setDetail(rs.getString("detail"));
							item.setPrice(rs.getInt("price"));
							item.setFileName(rs.getString("file_name"));
							item.setCategory(rs.getString("category"));
							item.setUserId(rs.getInt("user_id"));
							item.setAddDate(rs.getDate("create_date"));


							itemList.add(item);

						}

						System.out.println("カテゴリー検索による商品名検索");
						return itemList;

					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;

								}
							}
						}
					}

				//カテゴリー検索による商品総数の取得
				public static double geticCount(String category) throws SQLException {
					Connection con = null;
					PreparedStatement st = null;
					try {
						con = DBManager.getConnection();
						st = con.prepareStatement("select count(category) as cnt from item where category like '%"+category+"%'");

						ResultSet rs = st.executeQuery();
						double coung = 0.0;
						while (rs.next()) {
							coung = Double.parseDouble(rs.getString("cnt"));
						}

						System.out.println("カテゴリー検索による商品総数の取得");
						return coung;

					} catch (Exception e) {
						System.out.println(e.getMessage());
						throw new SQLException(e);
					} finally {
						if (con != null) {
							con.close();
						}
					}
				}

		//商品情報削除
		public static void itemDelete(int id) {
			Connection conn = null;

			 try {
				conn = DBManager.getConnection();

				String delsql = "DELETE FROM item WHERE id =?";

				PreparedStatement pStmt = conn.prepareStatement(delsql);

					pStmt.setInt(1, id);

					pStmt.executeUpdate();
					System.out.println("商品削除");

			} catch (SQLException e) {
				e.printStackTrace();
				} finally {
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
					}
				}
			}
		}

		//ユーザーIDで商品情報を取得
		public static ArrayList<ItemDataBeans> getItemtByuserId(int userId) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				st = con.prepareStatement(
						"SELECT * FROM item WHERE user_id = ?");
				st.setInt(1, userId);

				ResultSet rs = st.executeQuery();
				ArrayList<ItemDataBeans> buyDetailItemList = new ArrayList<ItemDataBeans>();

				while (rs.next()) {
					ItemDataBeans idb = new ItemDataBeans();

					idb.setId(rs.getInt("id"));
					idb.setName(rs.getString("name"));
					idb.setPrice(rs.getInt("price"));
					idb.setAddDate(rs.getDate("create_date"));
					idb.setFileName(rs.getString("file_name"));
					idb.setCategory(rs.getString("category"));
					idb.setDetail(rs.getString("detail"));


					buyDetailItemList.add(idb);
				}

				System.out.println("ユーザーIDで商品情報を取得");
				return buyDetailItemList;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}

		//新規商品登録
		public static void createItem(ItemDataBeans idb) {
			Connection conn = null;

			try {
				conn = DBManager.getConnection();

				String insql = "INSERT INTO item(price,name,category,detail,user_id,file_name,create_date)VALUES(?,?,?,?,?,?,now())";

					PreparedStatement pStmt = conn.prepareStatement(insql);

					pStmt.setInt(1, idb.getPrice());
					pStmt.setString(2, idb.getName());
					pStmt.setString(3, idb.getCategory());
					pStmt.setString(4, idb.getDetail());
					pStmt.setInt(5, idb.getUserId());
					pStmt.setString(6, idb.getFileName());

					pStmt.executeUpdate();

					System.out.println("新規商品登録");

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		public static ArrayList<ItemDataBeans> getRandItem(int limit) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				st = con.prepareStatement("SELECT * FROM item LEFT OUTER JOIN buy_detail ON item.id = buy_detail.item_id ORDER BY RAND() LIMIT ? ");
				st.setInt(1, limit);

				ResultSet rs = st.executeQuery();

				ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

				while (rs.next()) {
					ItemDataBeans idb = new ItemDataBeans();
					idb.setId(rs.getInt("id"));
					idb.setName(rs.getString("name"));
					idb.setPrice(rs.getInt("price"));
					idb.setAddDate(rs.getDate("create_date"));
					idb.setFileName(rs.getString("file_name"));
					idb.setCategory(rs.getString("category"));
					idb.setDetail(rs.getString("detail"));
					idb.setBuyId(rs.getInt("buy_id"));
					itemList.add(idb);
				}
				System.out.println("ランダムで商品を5件取得");
				return itemList;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}

		//商品情報更新
		public static void updateItem(ItemDataBeans item) {
			Connection conn = null;

			try {
				conn = DBManager.getConnection();

				String upsql = "UPDATE item SET name=?,price=?,category=?,detail=? WHERE id=?";

					PreparedStatement pStmt = conn.prepareStatement(upsql);

					pStmt.setString(1, item.getName());
					pStmt.setInt(2, item.getPrice());
					pStmt.setString(3, item.getCategory());
					pStmt.setString(4, item.getDetail());
					pStmt.setInt(5, item.getId());

					pStmt.executeUpdate();
					System.out.println("ユーザ情報更新完了");

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {

				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}



}