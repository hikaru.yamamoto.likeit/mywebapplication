package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemUpdateResult
 */
@WebServlet("/ItemUpdateResult")
public class ItemUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String category = request.getParameter("category");
		int itemId = Integer.parseInt(request.getParameter("itemId"));

		System.out.println(itemId+"確認２");

		ItemDataBeans item = new ItemDataBeans();

		item.setName(name);
		item.setPrice(Integer.parseInt(price));
		item.setDetail(detail);
		item.setCategory(category);
		item.setId(itemId);

		String confirmed = request.getParameter("confirm_button");

		switch (confirmed) {

		case "cancel":
			request.setAttribute("item", item);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemupdate.jsp");
			dispatcher.forward(request, response);
			break;

		case "regist":
			ItemDAO.updateItem(item);
			request.setAttribute("item", item);
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/itemupdateresult.jsp");
			dispatcher1.forward(request, response);

			break;
		}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	  }
	}