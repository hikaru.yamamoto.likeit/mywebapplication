package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class NewItemResult
 */
@WebServlet("/NewItemResult")
public class NewItemResult extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {

		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String category = request.getParameter("category");
		String userId = request.getParameter("id");
		String filename = request.getParameter("filename");

		ItemDataBeans idb = new ItemDataBeans();

		idb.setName(name);
		idb.setPrice(Integer.parseInt(price));
		idb.setDetail(detail);
		idb.setCategory(category);
		idb.setUserId(Integer.parseInt(userId));
		idb.setFileName(filename);


		String confirmed = request.getParameter("confirm_button");

		switch (confirmed) {

		case "cancel":
			request.setAttribute("idb", idb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newitem.jsp");
			dispatcher.forward(request, response);
			break;

		case "regist":

			ItemDAO.createItem(idb);

			request.setAttribute("idb", idb);
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/newitemresult.jsp");
			dispatcher1.forward(request, response);

			break;
		}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	  }

	public static boolean isNumber(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}