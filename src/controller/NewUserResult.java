package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class NewUserResult
 */
@WebServlet("/NewUserResult")
public class NewUserResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

		String name = request.getParameter("item");
		String address = request.getParameter("address");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		UserDataBeans udb =new UserDataBeans();

		udb.setName(name);
		udb.setAddress(address);
		udb.setLoginId(loginId);
		udb.setPassword(password);


		String confirmed = request.getParameter("confirm_button");

		switch (confirmed) {

		case "cancel":
			request.setAttribute("udb", udb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuser.jsp");
			dispatcher.forward(request, response);
			break;

		case "regist":
			UserDAO.createUser(udb);
			request.setAttribute("udb", udb);
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/newuserresult.jsp");
			dispatcher1.forward(request, response);

			break;
		}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	  }
	}