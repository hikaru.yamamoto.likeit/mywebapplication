package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.DeliveryDataBeans;
import beans.ItemDataBeans;
import dao.DeliveryDAO;

/**
 * Servlet implementation class BuyConfirm
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {

			int inputDeliveryId = Integer.parseInt(request.getParameter("delivery_id"));

			DeliveryDataBeans userSelectDMB = DeliveryDAO.getDeliveryDataBeansByID(inputDeliveryId);

			ArrayList<ItemDataBeans> cartIDBList = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			System.out.println(cartIDBList);

			int totalPrice = EcHelper.getTotalItemPrice(cartIDBList);

			BuyDataBeans bdb = new BuyDataBeans();
			bdb.setUserId((int) session.getAttribute("userId"));
			bdb.setTotalPrice(userSelectDMB.getPrice()+totalPrice);
			bdb.setDeliveryId(userSelectDMB.getId());
			bdb.setDeliveryName(userSelectDMB.getName());
			bdb.setDeliveryPrice(userSelectDMB.getPrice());

			System.out.println(bdb.getUserId()+"確認");

			//購入確定で利用
			session.setAttribute("bdb", bdb);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyconfirm.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
