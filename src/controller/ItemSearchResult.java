package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemSearchResult
 */
@WebServlet("/ItemSearchResult")
public class ItemSearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

		String searchWord = request.getParameter("search_word");

		String category = request.getParameter("category");

		System.out.println(category);

		ItemDAO itemdao = new ItemDAO();

		List<ItemDataBeans> itemList = itemdao.Itemfind(searchWord);
		List<ItemDataBeans> icList = itemdao.categoryfind(category);

		double itemCount = itemdao.getItemCount(searchWord);

		double icCount = itemdao.geticCount(category);

		request.setAttribute("itemList",itemList);
		request.setAttribute("itemCount",(int)itemCount);

		request.setAttribute("searchWord",searchWord);

		request.setAttribute("icList",icList);
		request.setAttribute("icCount",(int)icCount);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemsearchresult.jsp");
		dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}

	}
}
