package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BoardDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import dao.BoardDAO;
import dao.BuyDetailDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class BoardDelete
 */
@WebServlet("/BoardDelete")
public class BoardDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int commentId = Integer.parseInt(request.getParameter("comment_id"));

		int itemId = Integer.parseInt(request.getParameter("itemId"));


		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
		//対象のアイテム情報を取得
		ItemDataBeans item = ItemDAO.getItemByItemID(itemId);

		BoardDAO.commentDelete(commentId);

		BuyDetailDataBeans buydetail = BuyDetailDAO.getBuydetailByItemID(itemId);

		request.setAttribute("errMsg1", "売り切れ");
		request.setAttribute("buydetail", buydetail);

		request.setAttribute("errMsg", "コメントを削除しました");

		request.setAttribute("item", item);

		//コメント情報を取得
				ArrayList<BoardDataBeans> commentList = BoardDAO.getBoard(itemId);
		request.setAttribute("commentList", commentList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item.jsp");
		dispatcher.forward(request, response);


	} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
	}
	}

}
