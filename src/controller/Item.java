package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BoardDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.BoardDAO;
import dao.BuyDetailDAO;
import dao.ItemDAO;
import dao.UserDAO;

/**
 * Servlet implementation class Item
 */
@WebServlet("/Item")
public class Item extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {

			//選択された商品のIDを型変換し利用
			int itemId = Integer.parseInt(request.getParameter("item_id"));

			//対象のアイテム情報を取得
			ItemDataBeans item = ItemDAO.getItemByItemID(itemId);

			//コメントの情報を取得
			ArrayList<BoardDataBeans> commentList = BoardDAO.getBoard(itemId);

			//商品IDからユーザ名を取得
			UserDataBeans user = UserDAO.getItemIdByUsername(itemId);

			BuyDetailDataBeans buydetail = BuyDetailDAO.getBuydetailByItemID(itemId);

			System.out.println(buydetail.getBuyId());

			request.setAttribute("item", item);
			request.setAttribute("commentList", commentList);
			request.setAttribute("user", user);
			session.getAttribute("userInfo");

			if(commentList.size()==0) {
				request.setAttribute("errMsg5", "コメントはありません");
			}

			if (buydetail.getId() !=0) {
				request.setAttribute("errMsg1", "売り切れ");
				request.setAttribute("buydetail", buydetail);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item.jsp");
				dispatcher.forward(request, response);
				return;
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	  }
	}