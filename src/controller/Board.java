package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BoardDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import dao.BoardDAO;
import dao.BuyDetailDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class Board
 */
@WebServlet("/Board")
public class Board extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
		int itemId = Integer.parseInt(request.getParameter("itemId"));

		int userId = Integer.parseInt(request.getParameter("userId"));

		String comment = request.getParameter("comment");

		//対象のアイテム情報を取得
		ItemDataBeans item = ItemDAO.getItemByItemID(itemId);

		BuyDetailDataBeans buydetail = BuyDetailDAO.getBuydetailByItemID(itemId);

		if(comment.equals("")) {
			request.setAttribute("errMsg3", "コメントが未入力です");
			request.setAttribute("errMsg1", "売り切れ");
			request.setAttribute("buydetail", buydetail);


			request.setAttribute("item", item);

			//コメント情報を取得
			ArrayList<BoardDataBeans> commentList = BoardDAO.getBoard(itemId);

			request.setAttribute("commentList", commentList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item.jsp");
			dispatcher.forward(request, response);

		}

		if(!comment.equals("")) {

		BoardDAO.comment(itemId, userId, comment);
		request.setAttribute("item", item);
		request.setAttribute("errMsg1", "売り切れ");
		request.setAttribute("buydetail", buydetail);



		//コメント情報を取得
		ArrayList<BoardDataBeans> commentList = BoardDAO.getBoard(itemId);

		request.setAttribute("commentList", commentList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item.jsp");
		dispatcher.forward(request, response);
		}


	} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
	}
	}
}
