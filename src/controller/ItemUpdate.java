package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemUpdate
 */
@WebServlet("/ItemUpdate")
public class ItemUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			request.setCharacterEncoding("UTF-8");

			//選択された商品のIDを型変換し利用
			int itemId = Integer.parseInt(request.getParameter("item_id"));

			//対象のアイテム情報を取得
			ItemDataBeans item = ItemDAO.getItemByItemID(itemId);

			request.setAttribute("item", item);


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemupdate.jsp");
			dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {

		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String category = request.getParameter("category");
		int itemId = Integer.parseInt(request.getParameter("itemId"));

		System.out.println(itemId+"確認");


		ItemDataBeans item = new ItemDataBeans();
		item.setName(name);
		if ((!price.equals("")) || isNumber(price)) {
			item.setPrice(Integer.parseInt(price));
		}
		item.setDetail(detail);
		item.setCategory(category);
		item.setId(itemId);

		if (name.equals("") || price.equals("") || detail.equals("") || category.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			request.setAttribute("item", item);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemupdate.jsp");
			dispatcher.forward(request, response);
		}

		request.setAttribute("item", item);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemupdateconfirm.jsp");
		dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	  }



			public static boolean isNumber(String s) {
					try {
						Integer.parseInt(s);
						return true;
					} catch (NumberFormatException e) {
						return false;
					}
			}
}
