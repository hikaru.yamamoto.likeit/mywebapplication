package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);

	}
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {
			String loginId = request.getParameter("loginId");

			String password = request.getParameter("password");

			UserDAO userdao = new UserDAO();
			UserDataBeans user = userdao.LoginInfo(loginId, password);

			int userId = UserDAO.getUserId(loginId, password);

			System.out.println("ユーザーID　"+userId+"　を取得");


			if (user == null) {
				request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
				dispatcher.forward(request, response);
				return;
			}

			session.setAttribute("userId", userId);
			session.setAttribute("userInfo", user);

			System.out.println("ログイン");

			response.sendRedirect("Index");

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}

	}
}