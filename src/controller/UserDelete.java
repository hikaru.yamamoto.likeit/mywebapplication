package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserDelete
 */
@WebServlet("/UserDelete")
public class UserDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		UserDAO userdao = new UserDAO();
		UserDataBeans user = userdao.userRefe(Integer.parseInt(id));

		request.setAttribute("user",user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdelete.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {

		String id = request.getParameter("id");

		System.out.println(id);

		String confirmed = request.getParameter("confirm_button");

		switch (confirmed) {

		case "cancel":
			response.sendRedirect("Index");
			break;

		case "delete":

			System.out.println(id);
			UserDAO userdao = new UserDAO();
			userdao.userDelete(Integer.parseInt(id));

			session.removeAttribute("userInfo");
			session.removeAttribute("udb");

			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/userdeleteresult.jsp");
			dispatcher1.forward(request, response);

			break;
		}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	  }
	}