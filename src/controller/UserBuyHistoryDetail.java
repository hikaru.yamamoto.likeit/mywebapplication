package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class UserBuyHistoryDetail
 */
@WebServlet("/UserBuyHistoryDetail")
public class UserBuyHistoryDetail extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {

			String buyId =request.getParameter("buy_id");

			System.out.println(buyId);

			BuyDataBeans bdb = BuyDAO.getBuyDataBeansByBuyId(Integer.parseInt(buyId));

			System.out.println(bdb);

			ArrayList<ItemDataBeans> List = BuyDetailDAO.getItemDataBeansListByBuyId(Integer.parseInt(buyId));

			System.out.println(List);

			request.setAttribute("List", List);
			request.setAttribute("bdb", bdb);



			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userbuyhistorydetail.jsp");
			dispatcher.forward(request, response);

			} catch (NumberFormatException | SQLException e) {
				e.printStackTrace();
			}

		}
	}