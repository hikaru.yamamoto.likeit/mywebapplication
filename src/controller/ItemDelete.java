package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemDelete
 */
@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();


		UserDataBeans u = (UserDataBeans)session.getAttribute("userInfo");

		if (u == null) {

			response.sendRedirect("Login");
			return;
		}

		int itemId = Integer.parseInt(request.getParameter("item_id"));

		ItemDataBeans item = ItemDAO.getItemByItemID(itemId);

		request.setAttribute("item", item);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemdelete.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		int itemId = Integer.parseInt(request.getParameter("item_id"));

		String confirmed = request.getParameter("confirm_button");

		try {

		switch (confirmed) {

		case "cancel":
			response.sendRedirect("Index");
			break;

		case "delete":

			ItemDAO.itemDelete(itemId);
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/itemdeleteresult.jsp");
			dispatcher1.forward(request, response);

			break;
		}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	}
}