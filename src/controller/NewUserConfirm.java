package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class NewUserConfirm
 */
@WebServlet("/NewUserConfirm")
public class NewUserConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {

		UserDAO userdao = new UserDAO();

		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String loginId = request.getParameter("loginid");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");

		UserDataBeans udb =new UserDataBeans();

		udb.setName(name);
		udb.setAddress(address);
		udb.setLoginId(loginId);
		udb.setPassword(password);

		UserDataBeans user = userdao.findByLoginId(loginId);

		if(user != null||loginId.equals("")||name.equals("")||password.equals("")||password1.equals("")||address.equals("")||!password.equals(password1)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。もしくは既に使用されているログインIDです。");

			request.setAttribute("udb", udb);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuser.jsp");
			dispatcher.forward(request, response);

		}


		request.setAttribute("udb", udb);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuserconfirm.jsp");
		dispatcher.forward(request, response);

	} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");

	}
  }
}