package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		System.out.println(id+"get");

		UserDAO userdao = new UserDAO();
		UserDataBeans udb = userdao.userRefe(Integer.parseInt(id));

		request.setAttribute("udb",udb);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

		String id = request.getParameter("id");

		UserDAO userDao = new UserDAO();
		UserDataBeans uid = userDao.userRefe(Integer.parseInt(id));

		System.out.println(uid.getId()+"post");

		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");


		UserDataBeans udb =new UserDataBeans();

		UserDAO userdao = new UserDAO();
		UserDataBeans user = userdao.findByLoginId(loginId);


		if(user!=null||loginId.equals("")||name.equals("")||address.equals("")||password.equals("")||!password.equals(password1)) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。もしくは既に使用されているログインIDです。");

			request.setAttribute("name", name);
			request.setAttribute("address", address);
			request.setAttribute("loginId", loginId);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);

		}

			udb.setId(Integer.parseInt(id));
			udb.setName(name);
			udb.setAddress(address);
			udb.setLoginId(loginId);
			udb.setPassword(password);

			request.setAttribute("udb", udb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdateconfirm.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	  }
	}