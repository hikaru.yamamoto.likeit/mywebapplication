package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.ItemDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserData
 */
@WebServlet("/UserData")
public class UserData extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
		String userid = request.getParameter("id");

		UserDAO userdao = new UserDAO();
		UserDataBeans user = userdao.userRefe(Integer.parseInt(userid));

		List<ItemDataBeans> itemList = ItemDAO.getItemtByuserId(Integer.parseInt(userid));

		List<BuyDataBeans> ibList = BuyDAO.getHLByUserId(Integer.parseInt(userid));

		if(itemList.size()==0) {

			request.setAttribute("errMsg", "出品した商品はありません");

		}

		if(ibList.size()==0) {

			request.setAttribute("errMsg1", "購入した商品はありません");

		}

		request.setAttribute("user",user);

		request.setAttribute("itemList", itemList);

		request.setAttribute("ibList", ibList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdata.jsp");
		dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}