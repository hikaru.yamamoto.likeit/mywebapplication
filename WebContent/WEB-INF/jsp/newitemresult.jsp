<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>フリマサイト</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="parent clearfix titlebar">
		<div class="left">
            <a href="Index"><h2><font color="white">フリマサイト</font></h2></a>
            </div>

            <div class="right">

		<c:if test="${userInfo.loginId==null}">
			<i>ゲストさん</i>
			<i>　</i>
        	<a href="Login"><font color="white">ログイン</font></a>
        	<i>　</i>
        	<a href="NewUser"><font color="white">新規登録</font></a>

        </c:if>

		<c:if test="${userInfo.loginId!=null}">
        	<a>${userInfo.name}さん</a>
        	<i>　</i>
            <a href="NewItem"><font color="white">出品</font></a>
            <i>　</i>
			<a href="Cart"><font color="white">カート</font></a>
			<i>　</i>
			<a href="UserList"><font color="white">出品者一覧</font></a>
			<i>　</i>
			<a href="Logout"><font color="white">ログアウト</font></a>
		</c:if>

		</div>
	</div><br><br>

        <center>
          <h2>商品登録完了</h2><br>

    <table class="noborder" align="center" >
              <tr>
                  <td class="noborder" ><div class="card1 card-skin">
                      <div class="card__imgframe"><img src="img/${idb.fileName}" alt="画像" name="file" width="300" height="200"></div>
                        <div class="card__textbox">
                            <div class="card__titletext">商品名：${idb.name}</div>
                            <div class="card__overviewtext1">商品価格：${idb.price}円</div>
                            <div class="card__overviewtext1">商品カテゴリ：${idb.category}</div>
                            <div class="card__overviewtext1">商品説明：${idb.detail}</div>
                      </div></div></td>
            </table><br>

    <p>上記の内容で登録しました。</p>

             <a href="Index">TOPページ</a>
    <br>
    </center>

        <br><br>
            <footer class="page-footer brown">Made by OO</footer>
	</body>
</html>