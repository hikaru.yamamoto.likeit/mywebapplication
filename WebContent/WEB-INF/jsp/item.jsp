<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>フリマサイト</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="parent clearfix titlebar">
		<div class="left">
            <a href="Index"><h2><font color="white">フリマサイト</font></h2></a>
            </div>

            <div class="right">

		<c:if test="${userInfo.loginId==null}">
			<i>ゲストさん</i>
			<i>　</i>
        	<a href="Login"><font color="white">ログイン</font></a>
        	<i>　</i>
        	<a href="NewUser"><font color="white">新規登録</font></a>

        </c:if>

		<c:if test="${userInfo.loginId!=null}">
        	<a>${userInfo.name}さん</a>
        	<i>　</i>
            <a href="NewItem"><font color="white">出品</font></a>
            <i>　</i>
			<a href="Cart"><font color="white">カート</font></a>
			<i>　</i>
			<a href="UserList"><font color="white">出品者一覧</font></a>
			<i>　</i>
			<a href="Logout"><font color="white">ログアウト</font></a>
		</c:if>

		</div>
	</div><br><br>



      <center>
          <h2>商品詳細</h2><br>

          <c:if test="${userInfo.loginId==null}">
          <h5>購入するには<a href="Login">ログイン</a>が必要です。</h5>
          </c:if><br>

		<c:if test="${errMsg1 != null}" ><div class="alert alert-danger"  style="color:red" role="alert">${errMsg1}</div>
       		 </c:if>
         <table class="noborder" align="center" >
              <tr>
                  <td class="noborder" ><div class="card1 card-skin">
                      <div class="card__imgframe">
                      <img src="img/${item.fileName}" alt="画像" style="width:512px">
                      </div>
                        <div class="card__textbox">
                            <div class="card__titletext">商品名：${item.name}</div>
                            <div class="card__overviewtext1">出品者：${user.name}</div>
                            <div class="card__overviewtext1">商品価格：${item.formatPrice}円</div>
                            <div class="card__overviewtext1">商品カテゴリ：${item.category}</div>
                            <div class="card__overviewtext1">商品説明：<br>${item.detail}</div>
                            <div class="card__overviewtext1">出品日：<br>${item.formatDate}</div>
                      </div></div></td>
            </table>

          <br> <br>


          <table class="noborder" align="center" >

              <tr><c:if test="${userInfo.loginId=='admin'}">


				<form action="CartItemAdd" method="POST">
                  <td class="noborder" >
                  	<c:if test="${buydetail.id ==null}">
                  		<input type="hidden" name="itemId" value="${item.id}">
                  			<button type="submit" name="action">カートへ追加</button>
                  	</c:if>
                  </td>
              	</form>

                  <td class="noborder" >　　</td>

                  <td class="noborder" >
                  	<a href="ItemUpdate?item_id=${item.id}">
                  	<button type="submit">商品詳細編集</button></a>
                  	</td>

                  <td class="noborder" >　　</td>

                  <td class="noborder" >
                  	<a href="ItemDelete?item_id=${item.id}">
                  	<button type="submit">商品削除</button></a>
                  </td></c:if>


             <c:if test="${userInfo.loginId !='admin'&&buydetail.id == null&&userInfo.loginId !=null}">
				<form action="CartItemAdd" method="POST">
                  <td class="noborder" >
                  	<input type="hidden" name="itemId" value="${item.id}">
                  	<button type="submit" name="action">購入手続きへ</button>
                  </td>
              	</form>
            </c:if>

             <td class="noborder" >　　</td>

             <c:if test="${userInfo.id==item.userId && userInfo.loginId!='admin'}">
             	<td class="noborder" >
                  	<a href="ItemUpdate?item_id=${item.id}">
                  	<button type="submit">商品詳細編集</button></a>
                </td>
             </c:if>


              </tr>
          </table>

          <section>
          <br>

			<c:if test="${userInfo.loginId!=null}">
              <h3>コメント投稿</h3>

               <c:if test="${errMsg3 != null}" ><div class="alert alert-danger"  style="color:red" role="alert">${errMsg3}</div></c:if>

			<form action="Board" method="POST">
            	<p>名前:${userInfo.name}</p>

            	<input name="userId"type="hidden" value="${userInfo.id}">
            	<input name="itemId"type="hidden" value="${item.id}">

              <p>本文<br><textarea name="comment" cols="30" rows="1" maxlength="80" wrap="hard"></textarea></p>

              <input type="submit" value="投稿"><br><br>
             </form>

             </c:if>

          </section>


          <section>
              <h3>コメント一覧</h3>
               <c:if test="${errMsg5 != null}" ><div class="alert alert-danger"  style="color:red" role="alert">${errMsg5}</div></c:if>
	<center>
	 <c:if test="${errMsg != null}" ><div class="alert alert-danger"  style="color:red" role="alert">${errMsg}</div></c:if>
       		<br>
		<c:forEach var="cl" items="${commentList}">
				<c:if test="${cl.commentUserId != cl.hostUserId}">
                  <div class="card card-skin">
                        <div class="card__textbox">
                            <div class="card__overviewtext">質問時間：${cl.formatDate}</div>
                            <div class="card__overviewtext">質問者：${cl.userName}</div>
                            <div class="card__overviewtext">質問内容<br>${cl.commentDetail}</div>
                              <c:if test="${userInfo.id==item.userId && userInfo.loginId=='admin'}">

							<form action="BoardDelete" method="POST">
                          	  <input type="submit" value="削除">
                          	   <input name="comment_id"type="hidden" value="${cl.id}">
                          	   <input type="hidden" name="itemId" value="${item.id}">
                            </form>


                            </c:if>
                      </div></div><br></c:if>


          <br>

				<c:if test="${cl.commentUserId == cl.hostUserId}">
                  <div class="card card-skin">
                        <div class="card__textbox">
                            <div class="card__overviewtext">回答時間：${cl.formatDate}</div>
                            <div class="card__overviewtext">回答者：${cl.userName}</div>
                            <div class="card__overviewtext">回答内容<br>${cl.commentDetail}</div>
                              <c:if test="${userInfo.id==item.userId && userInfo.loginId=='admin'}">

                            <form action="BoardDelete" method="POST">
                          	  <input type="submit" value="削除">
                          	   <input name="comment_id"type="hidden" value="${cl.id}">
                          	   <input type="hidden" name="itemId" value="${item.id}">
                            </form>
                            </c:if>

                      </div></div><br></c:if>

			</c:forEach></center>

          </section>
      </center>

            <div class ="center">
            	<h5><a href="javascript:history.back()" >戻る</a></h5>
            </div>

         <br><br>
            <footer class="page-footer brown">Made by OO</footer>
	</body>
</html>