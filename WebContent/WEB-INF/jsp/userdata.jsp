<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>フリマサイト</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="parent clearfix titlebar">
		<div class="left">
            <a href="Index"><h2><font color="white">フリマサイト</font></h2></a>
            </div>

            <div class="right">

		<c:if test="${userInfo.loginId==null}">
			<i>ゲストさん</i>
			<i>　</i>
        	<a href="Login"><font color="white">ログイン</font></a>
        	<i>　</i>
        	<a href="NewUser"><font color="white">新規登録</font></a>

        </c:if>

		<c:if test="${userInfo.loginId!=null}">
        	<a>${userInfo.name}さん</a>
        	<i>　</i>
            <a href="NewItem"><font color="white">出品</font></a>
            <i>　</i>
			<a href="Cart"><font color="white">カート</font></a>
			<i>　</i>
			<a href="UserList"><font color="white">出品者一覧</font></a>
			<i>　</i>
			<a href="Logout"><font color="white">ログアウト</font></a>
		</c:if>

		</div>
	</div><br><br>

	<br>

		<center>
            <h4>ユーザ情報</h4>

            <p>ユーザ名：${user.name}</p>
            <p>ログインID：${user.loginId}</p>
            <p>住所：${user.address}</p><hr>



			<h4>出品商品一覧</h4><br>

			<c:if test="${errMsg != null}" ><div class="alert alert-danger"  style="color:red" role="alert">${errMsg}</div>
       		 </c:if>

       		 <c:if test="${errMsg == null}" >

						<table>
							<thead>
								<tr>
									<th style="width: 20%"></th>
									<th class="center">出品日時</th>
									<th class="center">商品名</th>
									<th class="center">商品価格</th>
								</tr>
							</thead>
						<c:forEach var="item" items="${itemList}">
							<tbody>
								<tr>
									<td class="center"><a href="Item?item_id=${item.id}">○　</a></td>
									<td class="center">${item.formatDate}</td>
									<td class="center">${item.name}</td>
									<td class="center">${item.formatPrice}円</td>
								</tr>
							</tbody>
						</c:forEach>

						</table><br></c:if>

						<hr>



	<c:if test="${userInfo.id == user.id||userInfo.loginId=='admin'}">

		<h4>購入履歴</h4><br>
		<c:if test="${errMsg1 != null}" ><div class="alert alert-danger"  style="color:red" role="alert">${errMsg1}</div>
       		 </c:if>

       		  <c:if test="${errMsg1 == null}" >
						<table>
							<thead>
								<tr>
									<th style="width: 20%"></th>
									<th class="center">購入日時</th>
									<th class="center">配送方法</th>
									<th class="center">購入金額</th>
								</tr>
							</thead>
						<c:forEach var="ib" items="${ibList}">
							<tbody>
								<tr>
									<td class="center"><a href="UserBuyHistoryDetail?buy_id=${ib.id}">○</a></td>
									<td class="center">${ib.formatDate}</td>
									<td class="center">${ib.deliveryName}</td>
									<td class="center">${ib.formatTotalPrice}円</td>
								</tr>
							</tbody>
						</c:forEach>
						</table></c:if>
						</c:if>

                        <br>   <a href="Index">TOPページ</a>
        </center>

        <br><br>
            <footer class="page-footer brown">Made by OO</footer>
	</body>
</html>