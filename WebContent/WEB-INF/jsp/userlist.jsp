<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>フリマサイト</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="parent clearfix titlebar">
		<div class="left">
            <a href="Index"><h2><font color="white">フリマサイト</font></h2></a>
            </div>

            <div class="right">

		<c:if test="${userInfo.loginId==null}">
			<i>ゲストさん</i>
			<i>　</i>
        	<a href="Login"><font color="white">ログイン</font></a>
        	<i>　</i>
        	<a href="NewUser"><font color="white">新規登録</font></a>

        </c:if>

		<c:if test="${userInfo.loginId!=null}">
        	<a>${userInfo.name}さん</a>
        	<i>　</i>
            <a href="NewItem"><font color="white">出品</font></a>
            <i>　</i>
			<a href="Cart"><font color="white">カート</font></a>
			<i>　</i>
			<a href="UserList"><font color="white">出品者一覧</font></a>
			<i>　</i>
			<a href="Logout"><font color="white">ログアウト</font></a>
		</c:if>

		</div>
	</div><br><br>



    <center>
      <br>
        <h2>出品者検索</h2><br>

	<form method="post" action="UserList">
			<table class="noborder" align="center" >
				<tr>
					<th class="noborder" >出品者　</th>
					<td class="noborder" ><input type="text" name="name" style="width:220px;"></td>
				<tr>
					<th class="noborder" >出品区域　</th>
					<td class="noborder" ><input type="text" name="address" style="width:220px;"></td>
			</table><br>

           <input type="submit" value="検索" >
     </form>


            <br>
            <hr>

		<h2>出品者一覧</h2><br>


			<table align="center">
    			<tr>
   				   <th class="s1-1">出品者</th>
   				   <th class="s1-1">　</th>
      			   <th class="s1-1">　</th>
    			</tr>

    		<c:forEach var="ul" items="${userList}">
    			<tr>
      				<td>${ul.name}</td>
        			<td>　</td>

				<c:if test="${userInfo.loginId=='admin'}">
      				<td>
      					<a href="UserData?id=${ul.id}"><input type="submit" class="btn" value="詳細";></a>
        				<a href="UserUpdate?id=${ul.id}"><input type="submit" class="btn" value="更新";></a>
						<a href="UserDelete?id=${ul.id}"><input type="submit" class="btn" value="削除";></a>
        			</td>
        		</c:if>

        		<c:if test="${userInfo.loginId !='admin'}">
        			<td>
        				<a href="UserData?id=${ul.id}"><input type="submit" class="btn" value="詳細";></a>
        			</td>


        		 <c:if test="${userInfo.loginId==ul.loginId}">
        			 <td>
        				<a href="UserUpdate?id=${ul.id}"><input type="submit" class="btn" value="更新";></a>
						<a href="UserDelete?id=${ul.id}"><input type="submit" class="btn" value="削除";></a>
        			</td>
        		 </c:if></c:if>

    			</tr>
    		</c:forEach>
  			</table>
            <br>
            <a href="Index">TOPページ</a>
        </center>


            <br><br>
            <footer class="page-footer brown">Made by OO</footer>

	</body>
</html>