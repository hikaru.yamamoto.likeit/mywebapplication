<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>フリマサイト</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="parent clearfix titlebar">
		<div class="left">
            <a href="Index"><h2><font color="white">フリマサイト</font></h2></a>
            </div>

            <div class="right">

		<c:if test="${userInfo.loginId==null}">
			<i>ゲストさん</i>
			<i>　</i>
        	<a href="Login"><font color="white">ログイン</font></a>
        	<i>　</i>
        	<a href="NewUser"><font color="white">新規登録</font></a>

        </c:if>

		<c:if test="${userInfo.loginId!=null}">
        	<a>${userInfo.name}さん</a>
        	<i>　</i>
            <a href="NewItem"><font color="white">出品</font></a>
            <i>　</i>
			<a href="Cart"><font color="white">カート</font></a>
			<i>　</i>
			<a href="UserList"><font color="white">出品者一覧</font></a>
			<i>　</i>
			<a href="Logout"><font color="white">ログアウト</font></a>
		</c:if>

		</div>
	</div><br><br>


		<h1 class="center">フリマサイト</h1><br>

			<div class="row center">
				<div class="input-field col s8 offset-s2">
					<form action="ItemSearchResult">
						<i>商品名検索</i>　<input type="text" name="search_word">
						<i>　　</i></form>

					<form action="ItemSearchResult">
						<i>カテゴリー検索</i>　
							<select	name="category">
								<c:forEach var="cl" items="${categoryList}">
									<option value="${cl.category}">${cl.category}</option>
								</c:forEach>
								<input type="submit" value="検索">
							</select>
					</form>
				</div>
			</div>


		<br><br>
		<h4 class="center">おすすめ商品</h4>
		<br>

			<ul class="float row">

				<c:forEach var="item" items="${itemList}">

					<li><a href="Item?item_id=${item.id}">
						<div class="card card-skin">
							<div class="card__imgframe">
							<img src="img/${item.fileName}" alt="画像" style="width:256px">
							</div>
							<div class="card__textbox">
								<div class="card__titletext">${item.name}</div>
								<div class="card__overviewtext">価格：${item.formatPrice}円</div>
								<div class="card__overviewtext">商品概要<br>${item.detail}</div>
								<div class="card__overviewtext">出品日<br>${item.formatDate}</div>

								<c:if test="${item.buyId !=0}">
								<div class="card__overviewtext"><font color="red">売り切れ</font></div>
								</c:if>

							</div>
						</div>
						</a>
					<p>　</p></li>
				</c:forEach>
			</ul>

		<br><div class="center"><a href="Index" >TOPページ</a></div>


	<br>
	<br>
	<footer class="page-footer brown">Made by OO</footer>

</body>
</html>