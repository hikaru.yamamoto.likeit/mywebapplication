<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>フリマサイト</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="parent clearfix titlebar">
		<div class="left">
            <a href="Index"><h2><font color="white">フリマサイト</font></h2></a>
            </div>

            <div class="right">

		<c:if test="${userInfo.loginId==null}">
			<i>ゲストさん</i>
			<i>　</i>
        	<a href="Login"><font color="white">ログイン</font></a>
        	<i>　</i>
        	<a href="NewUser"><font color="white">新規登録</font></a>

        </c:if>

		<c:if test="${userInfo.loginId!=null}">
        	<a>${userInfo.name}さん</a>
        	<i>　</i>
            <a href="NewItem"><font color="white">出品</font></a>
            <i>　</i>
			<a href="Cart"><font color="white">カート</font></a>
			<i>　</i>
			<a href="UserList"><font color="white">出品者一覧</font></a>
			<i>　</i>
			<a href="Logout"><font color="white">ログアウト</font></a>
		</c:if>

		</div>
	</div><br><br>


        <center>
       		 <c:if test="${errMsg != null}" ><div class="alert alert-danger"  style="color:red" role="alert">${errMsg}</div>
       		 </c:if><br>

      	  <h3>ログイン画面</h3><br>
        	<form action="Login" method="post">
          	 <p>ログインID　<input type="text" name="loginId" style="width:200px"></p>

          	 <p>パスワード　<input type="password" name="password" style="width:200px"></p>

         	  <p> <input type="submit" value="ログイン"></p>
		</form>

        </center>

        <div class="right">
            <a href="NewUser">ユーザ新規登録</a>
        </div>
        <br>
        <br>
            <footer class="page-footer brown">Made by OO</footer>

	</body>
</html>